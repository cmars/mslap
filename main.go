// mslap app slaps the crap with mmap
package main

import (
	"flag"
	"io"
	"launchpad.net/gommap"
	"log"
	"math/rand"
	"os"
	"runtime"
	"time"
)

var bs *int64 = flag.Int64("bs", 8192, "Block size")
var count *int64 = flag.Int64("count", 128, "Number of blocks")
var slappers *int = flag.Int("slappers", 16, "Concurrent slappers to run")
var path *string = flag.String("path", "mslap.img", "Memory-mapped file path")
var duration *int = flag.Int("duration", 60, "Duration of test in seconds")

var last []byte

func main() {
	flag.Parse()
	if *bs < 1 || *count < 1 || *slappers < 1 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	last = make([]byte, *count)
	err := os.Truncate(*path, (*bs)*(*count))
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile(*path, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	log.Println("Initializing map file")
	buf := make([]byte, *bs)
	for i := int64(0); i < *count; i++ {
		file.Write(buf)
	}
	log.Println("Done")
	mmap, err := gommap.Map(file.Fd(), gommap.PROT_READ|gommap.PROT_WRITE, gommap.MAP_SHARED)
	if err != nil {
		panic(err)
	}
	slapStop := make(chan interface{})
	slapWait := make(chan interface{})
	// Bring the pain
	for i := 0; i < *slappers; i++ {
		go slap(mmap, slapStop, slapWait)
	}
	log.Println("mslap stress test in progress...")

	time.Sleep(time.Duration(*duration) * time.Second)
	close(slapStop)
	log.Println("stopping slappers...")
	for i := 0; i < *slappers; i++ {
		<-slapWait
	}
	log.Println("Unmapping and closing test file")
	// Flush all changes
	err = mmap.Sync(gommap.MS_SYNC)
	if err != nil {
		panic(err)
	}
	// Unmap the file
	err = mmap.UnsafeUnmap()
	if err != nil {
		panic(err)
	}
	mmap = nil
	file.Close()
	if err != nil {
		panic(err)
	}
	log.Println("Verifying contents...")
	// Reopen for comparison
	file, err = os.Open(*path)
	if err != nil {
		panic(err)
	}
	// Compare contents of file with expected contents
	rc := 0
	for i := int64(0); i < *count; i++ {
		_, err = io.ReadFull(file, buf)
		if err == io.EOF {
			err = nil
			break
		} else if err != nil {
			panic(err)
		}
		for a := int64(0); a < *bs; a++ {
			if buf[a] != last[i] {
				log.Println("Block#", i, "Offset=", a, "Expected:", last[i], "Found:", buf[a])
				rc = 1
			}
		}
	}
	os.Exit(rc)
}

func slap(mmap gommap.MMap, slapStop chan interface{}, slapWait chan interface{}) {
	for {
		select {
		case _, ok := <-slapStop:
			if !ok {
				slapWait <- new(interface{})
				return
			}
		default:
			block := rand.Int63() % *count
			offset := block * (*bs)
			for i := int64(0); i < *bs; i++ {
				a := i + offset
				[]byte(mmap)[a] = []byte(mmap)[a] + 1
				if i == 0 {
					last[block] = []byte(mmap)[a]
				}
			}
		}
		// CPU-bound goroutine can starve the sleep otw,
		// http://stackoverflow.com/a/10096349
		runtime.Gosched()
	}
}
